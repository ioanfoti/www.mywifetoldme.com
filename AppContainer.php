<?php

use Pimple\Container;

class AppContainer extends Container
{
    protected static $instance;
    protected $root_path;
    protected $container;

    public function __construct()
    {
        parent::__construct();
        $this->root_path = __DIR__;
        $this->init();
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function init()
    {
        $this['root_path'] = function () {
            return $this->root_path;
        };

        $dotenv = new Dotenv\Dotenv(__DIR__);
        $dotenv->load();
        $dotenv->required([
            'ENVIRONMENT',
        ]);

        if (getenv('ENVIRONMENT') === 'development') {
            error_reporting(E_ALL);
            ini_set("display_errors", 1);
        }

        $this->register(new MyWife\ServiceProviders\ConfigProvider($this->root_path .'/files/config/'));
        
        $this->register(new MyWife\ServiceProviders\LoggerProvider($this->root_path .'/logs/'));

        $this->register(new MyWife\ServiceProviders\HttpClientProvider());
		
        $this->register(new MyWife\ServiceProviders\VisitorDetailsProvider(
            $this->root_path .'/files/maxmind_db/GeoLite2-City.mmdb',
            include $this->root_path .'/files/config/visitor_details.php'
        ));

        $this->register(new MyWife\ServiceProviders\FilesProvider());
        
        $this->register(new MyWife\ServiceProviders\CookiesProvider());
        
        $this->register(new MyWife\ServiceProviders\EnvironmentProvider());
        
        $this->register(new MyWife\ServiceProviders\EmailServiceProvider());
        
        $this->register(new MyWife\ServiceProviders\EmailDtoFactoryProvider());
        
        $this->register(new MyWife\ServiceProviders\VotingProvider());
        
        $this->register(new MyWife\ServiceProviders\EncryptionProvider());
        
        $this->register(new MyWife\ServiceProviders\WordpressRequestsProvider());
        
        $this->register(new MyWife\ServiceProviders\FormattersProvider());
        
        $this->register(new MyWife\ServiceProviders\SuggestionsProvider());
        
        $this->register(new MyWife\ServiceProviders\CaptchaProvider());
        
        $this->register(new MyWife\ServiceProviders\PredisProvider());
        
        $this->register(new MyWife\ServiceProviders\GeneralValuesProvider());
    }
}