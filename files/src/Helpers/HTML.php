<?php

class HTML
{
    /**
     * Generating a Link to a CSS File
     * @param $path
     * @param array $attributes
     */
    public static function style($path, array $attributes = null)
    {
        $relAttr = (isset($attributes['rel'])) ? '' : 'rel="stylesheet" ';
        echo PHP_EOL .'<link '. $relAttr . self::getAttributesString($attributes) .'href="'. $path .'">';
    }

    /**
     * Generating a Link to a Javascript File
     * @param $path
     * @param array $attributes
     */
    public static function script($path, array $attributes = null)
    {
        echo PHP_EOL .'<script '. self::getAttributesString($attributes) .'src="'. $path .'"></script>';
    }

    /**
     * Generating an HTML Image Element
     * @param $src
     * @param null $atr
     * @param array $attributes
     */
    public static function image($src, $atr = null, array $attributes = null)
    {
        echo PHP_EOL .'<img src="'. $src .'" '. self::getAttributesString($attributes) .'alt="'. $atr .'">';
    }

    /**
     * Convert attributes(array) to string
     * @param $attributes
     * @return string
     */
    private static function getAttributesString($attributes)
    {
        $attributesString = '';
        if (!empty($attributes)) {
            foreach ($attributes as $key => $val) {
                $attributesString .= $key .'="'. $val .'" ';
            }
        }
        return $attributesString;
    }
}