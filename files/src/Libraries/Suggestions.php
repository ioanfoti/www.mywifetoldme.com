<?php

namespace MyWife\Libraries;

class Suggestions
{
    private $messages;
    private $formMessages;
    private $db;
    private $logger;
    
    public function __construct()
    {
        $this->messages = \AppContainer::getInstance()['config']->getFile('messages');
        $this->formMessages = \AppContainer::getInstance()['config']->getFile('form_messages');
        $this->db = \AppContainer::getInstance()['config']->getFile('database')['wp'];
        $this->suggestionsConfig = \AppContainer::getInstance()['config']->getFile('suggestions');
        $this->logger = \AppContainer::getInstance()['logger'];
    }

    /**
     * Submit suggestion
     * @param array $suggestionData
     * @return array
     */
    public function submit($suggestionData)
    {        
        $result = $this->validate($suggestionData['post']);
        if (!empty($result['errors'])) {
            $result['msg'] = [
                'class' => 'col-md-6',
                'type'  => 'danger',
                'title' => $this->messages['errors_occured'],
                'list'  => $result['errors']
            ];
            return $result;
        }

        foreach($suggestionData['post'] as $key => $value) {
            $suggestionData['post'][$key] = $result['post'][$key];
        }
        
        // Start stopwatch
        $stopwatch = microtime(true);
        $logType = $this->suggestionsConfig['log']['default_type'];
        
        try {
            // Open connection with WP DB
            $connection = $this->openDbConnection();

            // Check if the user(by IP address) has already submitted this suggestion
            if ($this->checkSuggestionExistence($connection, $suggestionData) === true) {
                $result['msg'] = [
                    'class'         => 'col-md-6',
                    'type'          => 'warning',
                    'title'         => $this->messages['already_suggested'],
                    'paragraphs'    => [$this->messages['already_suggested_msg']]
                ];
                $logMsg['result'] = $this->suggestionsConfig['responses']['already_suggested'];
            }

            // Add suggestion to DB
            elseif ($this->addSuggestionToDb($connection, $suggestionData) === false) {
                $result['msg'] = [
                    'class'         => 'col-md-6',
                    'type'          => 'danger',
                    'title'         => $this->messages['errors_occured'],
                    'paragraphs'    => $result['errors']
                ];
                $logMsg['result'] = $this->suggestionsConfig['responses']['failed'];
                $logType = 'critical';
                
                // Send alert email
                $emailParams = [
                    'subject'   => 'Unable to submit suggestion - function Suggestions\submit',
                    'body'      => $e->getMessage(),
                ];
                $emailDto = \AppContainer::getInstance()['email_dto_factory']->createAlert($emailParams);
                \AppContainer::getInstance()['email_service']->send($emailDto);
            }

            else {
                $result['msg'] = [
                    'class'         => 'col-md-6',
                    'type'          => 'success',
                    'title'         => $this->messages['suggestion_sent'],
                    'paragraphs'    => [$this->messages['suggestion_sent_msg']]
                ];
                $logMsg['result'] = $this->suggestionsConfig['responses']['succeed'];
                
                // Send notification email
                $emailParams = [
                    'subject'       => 'Suggestion submitted',
                    'body'          => '<u>Post suggestion</u>: '. $suggestionData['post']['phrase'],
                    'parameters'    => [
                        'ip_address' => $suggestionData['ip_address'],
                        'country'    => $suggestionData['country'],
                    ],
                ];
                $emailDto = \AppContainer::getInstance()['email_dto_factory']->createNotification($emailParams);
                \AppContainer::getInstance()['email_service']->send($emailDto);
            }
        }
        catch (\PDOException $e) {
            $result['msg'] = [
                'class'         => 'col-md-6',
                'type'          => 'danger',
                'title'         => $this->messages['an_error_occured'],
                'paragraphs'    => [$this->messages['an_error_occured_msg']]
            ];
            
            $logMsg['result'] = $this->suggestionsConfig['responses']['connection_error'];
            $logType = 'critical';
            
            // Send alert email
            $emailParams = [
                'subject'   => 'Unable to connect to WP DB - function Suggestions\submit',
                'body'      => $e->getMessage(),
            ];
            $emailDto = \AppContainer::getInstance()['email_dto_factory']->createAlert($emailParams);
            \AppContainer::getInstance()['email_service']->send($emailDto);
        }
        
        // Stop stopwatch
        $logMsg['execution_time'] = (microtime(true) - $stopwatch) .'s';
        
        // Log voting result
        $this->logMessage($logType, $this->suggestionsConfig['log']['submit_description'], $logMsg + $suggestionData);
        
        return $result;
    }
    
    /**
     * Validate post values
     * @param array $post
     * @return array
     */
    private function validate($post)
    {
        $errors = [];
        $regex = \AppContainer::getInstance()['config']->getFile('regex')['english_characters'];
        
        $values = $this->getValuesForValidation($post);
        
        // Validate phrase
        if (empty($values['phrase']))
            $errors['phrase'] = sprintf($this->formMessages['required'], $this->messages['my_wife_told_me']);
        elseif (!preg_match($regex, $values['phrase']))
            $errors['phrase'] = sprintf($this->formMessages['english_characters'], $this->messages['my_wife_told_me']);
        elseif (strlen($values['phrase']) < 5 || strlen($values['phrase']) > 100)
            $errors['phrase'] = sprintf($this->formMessages['range'], $this->messages['my_wife_told_me'], 5, 100);
        
        // Validate email
        if (!empty($values['email'])) {
            if (filter_var($values['email'], FILTER_VALIDATE_EMAIL) === false)
                $errors['email'] = sprintf($this->formMessages['email'], $this->messages['email']);
        }
        
        // Validate tag
        if (!empty($values['tag'])) {
            if (!preg_match($regex, $values['tag']))
                $errors['tag'] = sprintf($this->formMessages['english_characters'], $this->messages['tag']);
            elseif (strlen($values['tag']) < 5 || strlen($values['tag']) > 32)
                $errors['tag'] = sprintf($this->formMessages['range'], $this->messages['tag'], 5, 32);
        }

        // Validate captcha
        if (empty($values['captcha']))
            $errors['captcha'] = sprintf($this->formMessages['required'], $this->messages['captcha']);
        elseif (strlen($values['captcha']) != 5 || $_SESSION['captcha'] != $values['captcha'])
            $errors['captcha'] = $this->formMessages['captcha'];

        $result = [
            'post' => [
                'phrase'    => $post['phrase'],
                'email'     => $post['email'],
                'tag'       => $post['tag'],
                'captcha'   => $post['captcha'],
            ],
            'errors' => $errors
        ];
        return $result;
    }
    
    /**
     * Get post values for validation
     * @param array $post
     * @return array
     */
    private function getValuesForValidation($post)
    {
        return [
            'phrase'    => isset($post['phrase']) ? trim(filter_var($post['phrase'], FILTER_SANITIZE_STRING)) : null,
            'email'     => isset($post['email']) ? trim(filter_var($post['email'], FILTER_SANITIZE_STRING)) : null,
            'tag'       => isset($post['tag']) ? trim(filter_var($post['tag'], FILTER_SANITIZE_STRING)) : null,
            'captcha'   => isset($post['captcha']) ? trim(filter_var($post['captcha'], FILTER_SANITIZE_STRING)) : null,
        ];
    }
    
    /**
     * Connect to WP database using PDO
     * @return \PDO
     */
    private function openDbConnection()
    {
        return new \PDO(
            'mysql:host='. $this->db['host'] .';'. 
            'dbname='. $this->db['name'], 
            $this->db['user'], 
            $this->db['pass']
        );
    }
    
    /**
     * Check if the user(by IP address) has already submitted this suggestion
     * @param $connection
     * @param array $suggestionData
     * @return bool
     */
    private function checkSuggestionExistence($connection, $suggestionData) 
    {
        $result = $connection->query("
            SELECT id
            FROM ". $this->suggestionsConfig['db']['table'] ."
            WHERE ip_address='". $suggestionData['ip_address'] ."'
            AND phrase='". $suggestionData['post']['phrase'] ."'
        ");

        return $result->rowCount() > 0 ? true : false;
    }

    /**
     * Insert suggestion to DB
     * @param $connection
     * @param array $suggestionData
     * @return bool
     */
    private function addSuggestionToDb($connection, $suggestionData) 
    {
        $result = $connection->query("
            INSERT INTO ". $this->suggestionsConfig['db']['table'] ." (ip_address, country, phrase, email, tag, date)
            VALUES ('". $suggestionData['ip_address'] ."', '". $suggestionData['country'] ."', '". $suggestionData['post']['phrase'] ."', '". $suggestionData['post']['email'] ."', '". $suggestionData['post']['tag'] ."', now())
        ");

        return $result->rowCount() > 0 ? true : false;
    }
    
    /**
     * Log message
     * @param string $type
     * @param string $description
     * @param array $data
     */
    private function logMessage($type, $description, $data)
    {
        $this->logger->{$type}($description, $data);
    }
}