<?php

namespace MyWife\Libraries;

class Formatters
{
    /**
     * Sort multidimensional array based on key
     * @param array $array
     * @param string $sortKey
     * @param string $order
     * @return array
     */
    public function sortMultiArray($array, $sortKey, $order = SORT_ASC)
    {
        $result = $sortableArray = [];

        if (count($array) > 0) {
            foreach ($array as $mainKey => $mainVal) {
                if (is_array($mainVal))
                    foreach ($mainVal as $subKey => $subVal) {
                        if ($subKey == $sortKey)
                            $sortableArray[$mainKey] = $subVal;
                    }
                else
                    $sortableArray[$mainKey] = $mainVal;
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortableArray);
                    break;
                case SORT_DESC:
                    arsort($sortableArray);
                    break;
            }

            foreach ($sortableArray as $key => $val) {
                $result[$key] = $array[$key];
            }
        }

        return $result;
    }
    
    /**
     * Convert hex color to rgb
     * @param string $hex
     * @return array
     */
    public function hexToRgb($hex) 
    {
        $hexCode = str_replace("#", "", $hex);

        if(strlen($hexCode) == 3) {
            $r = hexdec(substr($hexCode, 0, 1).substr($hexCode, 0, 1));
            $g = hexdec(substr($hexCode, 1, 1).substr($hexCode, 1, 1));
            $b = hexdec(substr($hexCode, 2, 1).substr($hexCode, 2, 1));
        } 
        else {
            $r = hexdec(substr($hexCode, 0, 2));
            $g = hexdec(substr($hexCode, 2, 2));
            $b = hexdec(substr($hexCode, 4, 2));
        }
        $rgb = [
            'red'   => $r,
            'green' => $g,
            'blue'  => $b,
        ];

        return $rgb;
    }
}