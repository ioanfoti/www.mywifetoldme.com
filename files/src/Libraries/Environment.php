<?php

namespace MyWife\Libraries;

class Environment
{    
    /**
     * Get server http referer
     * @return string
     */
    public function getHttpReferer()
    {
        return !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
    }
    
    /**
     * Get server request uri
     * @return string
     */
    public function getRequestUri()
    {
        return empty($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
    }
    
    /**
     * Get server name value
     * @return string
     */
    public function getServerName()
    {
        $serverName = $_SERVER['SERVER_NAME'];

        // For subdomains
        if (strpos($serverName, 'www.') === false) {

            // Explode server name
            $parts = explode('.', $serverName);

            // Replace subdomain with www
            array_splice($parts, -3, 1, 'www');

            // Create server name
            $serverName = implode('.', $parts);
        }
        return $serverName;
    }
    
    /**
     * Get domain
     * @return string
     */
    public function getDomain()
    {
        return str_replace('www.', '', strstr($this->getServerName(), 'www.'));
    }
    
    /**
     * Get name of the environment
     * @return string
     */
    public function getName()
    {
        return getenv('ENVIRONMENT');
    }
    
    /**
     * Get host
     * @return string
     */
    public function getHost()
    {
        return "http://". $this->getServerName();
    }
    
    /**
     * Get secure host
     * @return string
     */
    public function getSecureHost()
    {
        $host = $this->getHost();
        return ($this->getName() != 'production') ? $host : str_replace('http:', 'https:', $host);
    }
    
    /**
     * Get home link
     * @return string
     */
    public function getHomeLink()
    {
        return $this->getHost();
    }
    
    /**
     * Get secure home link
     * @return string
     */
    public function getSecureHomeLink()
    {
        return $this->getSecureHost();
    }
    
    /**
     * Get file hosting path
     * @return string
     */
    public function getFileHostingPath()
    {
        return $this->getHost();
    }
}