<?php

namespace MyWife\Libraries;

use MyWife\Dto\EmailDto;

class EmailDtoFactory
{
    private $emailConfig;

    public function __construct()
    {
        $this->emailConfig = \AppContainer::getInstance()['config']->getFile('email');
    }

    /**
     * Create alert email
     * @param array $params
     * @return EmailDto
     */
    public function createAlert($params)
    {
        $emailDto = new EmailDto();
        
        $emailDto->setSubject($params['subject']);
        $emailDto->setTo([
            $this->emailConfig['contact']['admin']['email'] => $this->emailConfig['contact']['admin']['name']
        ]);
        $emailDto->setBody($params['body']);
        $emailDto->setTemplate(\AppContainer::getInstance()['general_values']->get()['root_path'] .'/files/views/emails/templates/alert.php');
        
        return $emailDto;
    }

    /**
     * Create notification email
     * @param array $params
     * @return EmailDto
     */
    public function createNotification($params)
    {
        $emailDto = new EmailDto();
        
        $emailDto->setSubject($params['subject']);
        $emailDto->setTo([
            $this->emailConfig['contact']['admin']['email'] => $this->emailConfig['contact']['admin']['name']
        ]);
        $emailDto->setBody($params['body']);
        $emailDto->setParameters($params['parameters']);
        $emailDto->setTemplate(\AppContainer::getInstance()['general_values']->get()['root_path'] .'/files/views/emails/templates/notification.php');
        
        return $emailDto;
    }
}