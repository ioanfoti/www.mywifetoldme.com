<?php

namespace MyWife\Libraries;

class Captcha
{
    private $captchaConfig;

    public function __construct()
    {
        $this->captchaConfig = \AppContainer::getInstance()['config']->getFile('captcha');
    }
    
    /**
     * Create captcha string 
     * @return string
     */
    public function createString()
    {
        $string = '';
        for ($i=0; $i < $this->captchaConfig['number_of_characters']; $i++) {
            $string .= chr(rand(97, 122));
        }
        return $string;
    }
    
    /**
     * Creeate captcha image
     * @param type $string
     * @return image resource identifier
     */
    public function createImage($string)
    {
        $image = imagecreatetruecolor($this->captchaConfig['img_width'], $this->captchaConfig['img_height']);
        
        $rgbBackground = \AppContainer::getInstance()['formatters']->hexToRgb($this->captchaConfig['background_color']);
        $backgroundColor = imagecolorallocate($image, $rgbBackground['red'], $rgbBackground['green'], $rgbBackground['blue']);
        
        $rgbText = \AppContainer::getInstance()['formatters']->hexToRgb($this->captchaConfig['text_color']);
        $textColor = imagecolorallocate($image, $rgbText['red'], $rgbText['green'], $rgbText['blue']);

        imagefilledrectangle(
            $image, 
            $this->captchaConfig['coordinates']['x1'], 
            $this->captchaConfig['coordinates']['y1'], 
            $this->captchaConfig['coordinates']['x2'], 
            $this->captchaConfig['coordinates']['y2'], 
            $backgroundColor
        );
        
        $font = \AppContainer::getInstance()['general_values']->get()['root_path'] .'/files/assets/fonts/'. $this->captchaConfig['image_text']['font_name'];
        imagettftext(
            $image, 
            $this->captchaConfig['image_text']['font_size'], 
            $this->captchaConfig['image_text']['angle'], 
            $this->captchaConfig['image_text']['x_ordinate'], 
            $this->captchaConfig['image_text']['y_ordinate'], 
            $textColor, 
            $font, 
            $string
        );
        
        return $image;
    }
}