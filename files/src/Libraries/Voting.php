<?php

namespace MyWife\Libraries;

class Voting
{
    private $db;
    private $votingConfig;
    private $logger;

    public function __construct()
    {
        $this->logger = \AppContainer::getInstance()['logger'];
        $this->db = \AppContainer::getInstance()['config']->getFile('database')['wp'];
        $this->votingConfig = \AppContainer::getInstance()['config']->getFile('voting');
    }
    
    /**
     * Get voting data for post
     * @param string $postId
     * @param string $visitorIp
     * @param string $countryCode
     * @return array
     */
    public function getVotingPostData($postId, $visitorIp, $countryCode)
    {
        try {
            // Open connection with WP DB
            $connection = $this->openDbConnection();

            // Get voting data from database
            $dbVotingData = $this->getDbVotingData($connection, $postId);

            // Process & save voting data
            $votingData = $this->processDbVotingData($dbVotingData, $visitorIp);
            
            // Get data for voting buttons
            $votingData = $votingData + $this->getButtonsData($postId, $visitorIp, $countryCode, $votingData['already_voted']);
            
            // Close connection
            $connection = null;
        }
        catch (\PDOException $e) {
            $logData = [
                'post_id'       => $postId,
                'visitor_ip'    => $visitorIp,
                'error'         => $this->votingConfig['log']['connection_error'] . $e->getMessage(),
            ];
            $this->logMessage('critical', $this->votingConfig['log']['get_votes_description'], $logData);
            
            // Send alert email
            $emailParams = [
                'subject'   => 'Unable to connect to WP DB - function Voting\getVotingPostData',
                'body'      => $e->getMessage(),
            ];
            $emailDto = \AppContainer::getInstance()['email_dto_factory']->createAlert($emailParams);
            \AppContainer::getInstance()['email_service']->send($emailDto);
        }

        return $votingData;
    }
    
    /**
     * Get voting data for every voting post (published)
     * @param string $visitorIp
     * @param string $countryCode
     * @return array
     */
    public function getAllVotingPostsData($visitorIp, $countryCode) 
    {
        $votingPosts = \AppContainer::getInstance()['wordpress']->getVotingPosts();

        $i = 0;
        foreach($votingPosts as $post) {
            $votingData[$i] = $this->getVotingPostData($post['ID'], $visitorIp, $countryCode);
            $votingData[$i]['url'] = $post['post_name'];
            $votingData[$i]['title'] = $post['post_title'];
            $votingData[$i]['publish_date'] = strstr($post['post_date_gmt'], ' ', true);
            $i++;
        }
        
        return $votingData;
    }
    
    /**
     * Decrypt posted voting data
     * @param string $encryptedData
     * @return array
     */
    public function decryptData($encryptedData)
    {
        $decryptedData = \AppContainer::getInstance()['encryption']->decrypt($encryptedData);
        $decryptedDataItems = explode('+', $decryptedData);
        $result = [
            'post_id'       => !empty($decryptedDataItems[0]) ? $decryptedDataItems[0] : null,
            'ip_address'    => !empty($decryptedDataItems[1]) ? $decryptedDataItems[1] : null,
            'country'       => !empty($decryptedDataItems[2]) ? $decryptedDataItems[2] : null,
            'vote_type'     => !empty($decryptedDataItems[3]) ? $decryptedDataItems[3] : null,
        ];
        return $result;
    }
    
    /**
     * Validate voting data
     * @param array $votingData
     * @return boolean
     */
    public function validateData($votingData)
    {
        $badDataResult = $this->votingConfig['responses']['bad_request'];
        if (empty($votingData['post_id']) || !ctype_digit($votingData['post_id']))
            return $badDataResult;
        
        if (empty($votingData['ip_address']) || filter_var($votingData['ip_address'], FILTER_VALIDATE_IP) === false)
            return $badDataResult;
        
        if (empty($votingData['country']) || strlen($votingData['country']) != 2)
            return $badDataResult;
        
        if (empty($votingData['vote_type']) || !in_array($votingData['vote_type'], $this->votingConfig['available_vote_types']))
            return $badDataResult;
        
        return $this->votingConfig['responses']['valid_data'];
    }
    
    /**
     * Submit vote
     * @param array $votingData
     * @return string
     */
    public function submit($votingData)
    {
        // Start stopwatch
        $stopwatch = microtime(true);
        $logType = $this->votingConfig['log']['default_type'];
        
        try {
            // Open connection with WP DB
            $connection = $this->openDbConnection();
        
            // Check if post exists & uses voting template
            if ($this->checkPostExistence($connection, $votingData) === false) {
                $result = $this->votingConfig['responses']['post_not_exist'];
                $logMsg['result'] = sprintf($this->votingConfig['log']['post_not_exist'], $votingData['post_id']);
            }
            // Check if the user(by IP address) has already voted for post(Post ID)
            elseif(!empty($this->checkVoteExistence($connection, $votingData))) {
                $result = $this->votingConfig['responses']['already_voted'];
                $result['voteClass'] = $this->checkVoteExistence($connection, $votingData) == 'already_heard' ? 'up' : 'down';
                $logMsg['result'] = $result['text'];
            }
            
            // Add vote to DB
            elseif ($this->addVoteToDb($connection, $votingData) === false) {
                $result = $this->votingConfig['responses']['failed'];
                $logMsg['result'] = $result['text'];
                $logType = 'error';
            }
            
            else {
                $result = $this->votingConfig['responses']['succeed'];
                $logMsg['result'] = $result['text'];
            }
            
            // Close connection
            $connection = null;
        }
        catch (\PDOException $e) {
            $result = $this->votingConfig['responses']['connection_error'];
            $logMsg['result'] = $this->votingConfig['log']['connection_error'] . $e->getMessage();
            $logType = 'critical';
            
            // Send alert email
            $emailParams = [
                'subject'   => 'Unable to connect to WP DB - function Voting\submit',
                'body'      => $e->getMessage(),
            ];
            $emailDto = \AppContainer::getInstance()['email_dto_factory']->createAlert($emailParams);
            \AppContainer::getInstance()['email_service']->send($emailDto);
        }

        // Stop stopwatch
        $logMsg['execution_time'] = (microtime(true) - $stopwatch) .'s';
        
        // Log voting result
        $this->logMessage($logType, $this->votingConfig['log']['submit_description'], $logMsg + $votingData);
        
        return $result;
    }

    /**
     * Connect to WP database using PDO
     * @return \PDO
     */
    private function openDbConnection()
    {
        return new \PDO(
            'mysql:host='. $this->db['host'] .';'. 
            'dbname='. $this->db['name'], 
            $this->db['user'], 
            $this->db['pass']
        );
    }
    
    /**
     * Get votes for post
     * @param $connection
     * @param string $postId
     * @return array
     */
    private function getDbVotingData($connection, $postId)
    {
        return $connection->query("
            SELECT ip_address, vote_type 
            FROM ". $this->votingConfig['db']['voting_table'] ."
            WHERE post_id =$postId
        ");
    }
    
    /**
     * Process voting data
     * @param array $votingData
     * @return array
     */
    private function processDbVotingData($votingData, $visitorIp)
    {
        $dbVotingData = [
            'already_heard' => 0,
            'not_yet'       => 0,
            'already_voted' => '',
        ];
        
        foreach($votingData as $row) {
            if ($row['vote_type'] == 'already_heard')
                $dbVotingData['already_heard']++;
            else
                $dbVotingData['not_yet']++;

            if ($row['ip_address'] == $visitorIp)
                $dbVotingData['already_voted'] = $row['vote_type'];
        }
        
        return $dbVotingData;
    }
    
    /**
     * Get data for voting buttons
     * @param string $postId
     * @param string $visitorIp
     * @param string $countryCode
     * @param string $alreadyVoted
     * @return array
     */
    public function getButtonsData($postId, $visitorIp, $countryCode, $alreadyVoted)
    {
        if (empty($alreadyVoted)) {
            $buttonsData['voting_script'] = true;
            $buttonsData['encrypt_up_vote'] = \AppContainer::getInstance()['encryption']->encrypt(
                $postId .'+'. 
                $visitorIp .'+'. 
                $countryCode .'+'.
                'already_heard'
            );
            $buttonsData['encrypt_down_vote'] = \AppContainer::getInstance()['encryption']->encrypt(
                $postId .'+'. 
                $visitorIp .'+'. 
                $countryCode .'+'.
                'not_yet'
            );
        }
        else {
            $buttonsData['up_voted'] = $buttonsData['down_voted'] = '';
            if ($alreadyVoted == 'already_heard')
                $buttonsData['up_voted'] = 'voted';
            else
                $buttonsData['down_voted'] = 'voted';
        }
        
        return $buttonsData;
    }
    
    /**
     * Check if given post exist & uses voting template
     * @param $connection
     * @param array $votingData
     * @return bool
     */
    private function checkPostExistence($connection, $votingData)
    {
        $result = $connection->query("
            SELECT meta_id
            FROM ". $this->votingConfig['db']['postmeta_table'] ."
            WHERE post_id=". $votingData['post_id'] ."
            AND meta_value='". $this->votingConfig['db']['voting_template'] ."'
        ");

        return $result->rowCount() > 0 ? true : false;
    }
        
    /**
     * Check if the user(by IP address) has already voted for post(Post ID)
     * @param $connection
     * @param array $votingData
     * @return bool
     */
    private function checkVoteExistence($connection, $votingData) 
    {
        $result = $connection->query("
            SELECT vote_type
            FROM ". $this->votingConfig['db']['voting_table'] ."
            WHERE post_id=". $votingData['post_id'] ."
            AND ip_address='". $votingData['ip_address'] ."'
        ");

        $response = '';
        if ($result->rowCount() > 0) {
            $vote = $result->fetch();
            $response = $vote['vote_type'];
        }
        return $response;
    }
   
    /**
     * Insert vote to voting table
     * @param $connection
     * @param array $votingData
     * @return bool
     */
    private function addVoteToDb($connection, $votingData) 
    {
        $result = $connection->query("
            INSERT INTO ". $this->votingConfig['db']['voting_table'] ." (post_id, ip_address, country, vote_type, vote_date)
            VALUES (". $votingData['post_id'] .", '". $votingData['ip_address'] ."', '". $votingData['country'] ."', '". $votingData['vote_type'] ."', now())
        ");

        return $result->rowCount() > 0 ? true : false;
    }

    /**
     * Log message
     * @param string $type
     * @param string $description
     * @param array $data
     */
    private function logMessage($type, $description, $data)
    {
        $this->logger->{$type}($description, $data);
    }
}