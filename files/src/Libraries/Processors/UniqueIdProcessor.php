<?php 

namespace MyWife\Libraries\Processors;

class UniqueIdProcessor
{
    private $uniqueId;

    public function __construct($uniqueId)
    {
        $this->uniqueId = $uniqueId;
    }

    public function __invoke(array $record)
    {
        $record['extra']['uniqueId'] = $this->uniqueId;

        return $record;
    }
}