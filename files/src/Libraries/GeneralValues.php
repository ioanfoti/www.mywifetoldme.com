<?php

namespace MyWife\Libraries;

class GeneralValues
{
    public function __construct()
    {
        
    }

    /**
     * Get general values
     * @return array
     */
    public function get()
    {
        $general['root_path'] = \AppContainer::getInstance()['root_path'];
        $general['ip_address'] = \AppContainer::getInstance()['visitor_details']->getIp();
        $general['country_code'] = \AppContainer::getInstance()['visitor_details']->getCountryCode();
        $general['country_name'] = \AppContainer::getInstance()['visitor_details']->getCountryName();
        $general['file_versions'] = \AppContainer::getInstance()['files']->getVersions();
        $general['cookies_parameters'] = \AppContainer::getInstance()['cookies']->getParameters();
        $general['http_referer'] = \AppContainer::getInstance()['environment']->getHttpReferer();
        $general['request_uri'] = \AppContainer::getInstance()['environment']->getRequestUri();
        $general['server_name'] = \AppContainer::getInstance()['environment']->getServerName();
        $general['domain'] = \AppContainer::getInstance()['environment']->getDomain();
        $general['environment'] = \AppContainer::getInstance()['environment']->getName();
        $general['host'] = \AppContainer::getInstance()['environment']->getHost();
        $general['secure_host'] = \AppContainer::getInstance()['environment']->getSecureHost();
        $general['home_link'] = \AppContainer::getInstance()['environment']->getHomeLink();
        $general['secure_home_link'] = \AppContainer::getInstance()['environment']->getSecureHomeLink();
        $general['file_hosting_path'] = \AppContainer::getInstance()['environment']->getFileHostingPath();

        return $general;
    }
}