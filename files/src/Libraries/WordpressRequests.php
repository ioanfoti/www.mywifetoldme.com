<?php

namespace MyWife\Libraries;

class WordpressRequests
{
    private $wordpressConfig;
    
    public function __construct()
    {
        $this->wordpressConfig = $this->encryptionData = \AppContainer::getInstance()['config']->getFile('wordpress');
    }
    
    /**
     * Get published voting posts
     * @param string $numberOfPosts
     * @return array
     */
    public function getVotingPosts($numberOfPosts = null)
    {
        return $this->getPosts('voting', $numberOfPosts);
    }
    
    /**
     * Get posts for given category
     * @param string $category
     * @param string $numberOfPosts If null returns all posts for the category
     * @return array
     */
    private function getPosts($category, $numberOfPosts = null)
    {
        $args = $this->wordpressConfig['args'][$category];
        if (!empty($numberOfPosts))
            $args['posts_per_page'] = $numberOfPosts;
        
        query_posts($args);

        if (have_posts()) {
            while (have_posts()) {
                the_post();
                $post[] = (array) get_post();
            }
        }
        return $post;
    }
}