<?php

namespace MyWife\Libraries;

class Files
{
    /**
     * Get versions of included files (css, js etc)
     * @return array
     */
    public function getVersions()
    {
        return \AppContainer::getInstance()['config']->getFile('file_versions');
    }
}