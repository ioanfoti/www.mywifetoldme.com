<?php

namespace MyWife\Libraries;

class Cookies
{
    /**
     * Get basic cookies parameters 
     * @return array
     */
    public function getParameters()
    {
        $parameters = \AppContainer::getInstance()['config']->getFile('cookies');
        $serverName = \AppContainer::getInstance()['environment']->getServerName();
        $parameters['domain'] = preg_replace('/^www/', '', strstr($serverName, 'www.'));
        return $parameters;
    }
}