<?php

namespace MyWife\Libraries;

class Config
{
    private $configFolder;
    
    /**
     * @param string $configFolder Path to configuration folder
     */
    public function __construct($configFolder)
    {
        $this->configFolder = $configFolder;
    }
    
    /**
     * Get requested configuration files from defined configuration folder
     * @return array
     */
    public function getFile($fileName)
    {
        $fileName = strpos($fileName, '.php') === false ? $fileName .'.php' : $fileName;
        $filePath =  $this->configFolder . $fileName;

        if (file_exists($filePath) === false)
            throw new \Exception ("Requested configuration file ". $filePath ." does not exist", 404);
        
        $config = include $filePath;
        return $config;
    }
}