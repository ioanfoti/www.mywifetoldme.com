<?php

namespace MyWife\Libraries;

use GeoIp2\Database\Reader;

class VisitorDetails
{
    protected static $instance;
    private $cityDb;
    private $config;
    private $logger;
    private $failedRequest;
    private $ip;
    private $details;
    private $countryCode;
    private $countryName;
    private $city;
    private $postalCode;
    private $latitude;
    private $longitude;

    public function __construct($cityDb)
    {
        $this->cityDb = $cityDb;
        $this->config = \AppContainer::getInstance()['config']->getFile('visitor_details');
        $this->failedRequest = false;
        $this->logger = \AppContainer::getInstance()['logger'];
        $this->init();
    }

    public static function getInstance($cityDb)
    {
        if (self::$instance === null) {
            self::$instance = new self($cityDb);
        }

        return self::$instance;
    }

    /**
     * Initialize object
     */
    private function init()
    {
        $this->setIp();
        $this->setDetails();
        $this->setCountryCode();
        $this->setCountryName();
        $this->setCity();
        $this->setPostalCode();
        $this->setLatitude();
        $this->setLongitude();
    }
    
    /**
     * Get visitor's IP
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }
    
    /**
     * Set visitor's IP
     */
    private function setIp()
    {
        $this->ip = $ip = '';

        // $_SERVER keys for IP
        $serverData = array(
            'HTTP_CLIENT_IP',
            'HTTP_X_FORWARDED_FOR',
            'HTTP_X_FORWARDED',
            'HTTP_X_CLUSTER_CLIENT_IP',
            'HTTP_FORWARDED_FOR',
            'HTTP_FORWARDED',
            'REMOTE_ADDR'
        );

        // Get IP from first key detected
        foreach($serverData as $key) {
            if(array_key_exists($key, $_SERVER) === true) {
                foreach(explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip);
                    if (!empty($ip)) {
                        if (getenv('ENVIRONMENT') === 'development' && filter_var($ip, FILTER_VALIDATE_IP)) {
                            $this->ip = $ip;
                            break;
                        }
                        elseif (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                            $this->ip = $ip;
                            break;
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Set object with location details for given IP address
     */
    private function setDetails()
    {
        if (getenv('ENVIRONMENT') === 'development') {
            $this->failedRequest = true;
            return;
        }

        $reader = new Reader($this->cityDb);
        
        try 
        {
            $this->details = $reader->city($this->getIp());
        } 
        catch (\Exception $e) 
        {
            $this->failedRequest = true;
            
            // Log failed request
            $logMsg = [
                'ip'        => $this->ip,
                'code'      => $e->getCode(),
                'message'   => $e->getMessage(),
                'name'      => get_class($e),
            ];
            $this->logger->info("Get details for IP failed", $logMsg);
        }
    }
    
    /**
     * Get country code in ISO 3166-1 alpha-2 for IP
     * @return string
     */
    public function getCountryCode() 
    {
        return $this->countryCode;
    }
    
    /**
     * Set country code in ISO 3166-1 alpha-2 for IP
     */
    private function setCountryCode() 
    {
        $this->countryCode = $this->failedRequest ? $this->config['default_values']['country_code'] : $this->details->country->isoCode;
    }
    
    /**
     * Get country name for IP
     * @return string
     */
    public function getCountryName() 
    {
        return $this->countryName;
    }
    
    /**
     * Get country name for IP
     */
    private function setCountryName() 
    {
        $this->countryName = $this->failedRequest ? $this->config['default_values']['country_name'] : $this->details->country->name;
    }
    
    /**
     * Get city name for IP
     * @return string
     */
    public function getCity() 
    {
        return $this->city;
    }
    
    /**
     * Set city name for IP
     */
    private function setCity() 
    {
        $this->city = $this->failedRequest ? $this->config['default_values']['city'] : $this->details->city->name;
    }
    
    /**
     * Get postal code for IP
     * @return string
     */
    public function getPostalCode() 
    {
        return $this->postalCode;
    }

    /**
     * Set postal code for IP
     */
    private function setPostalCode() 
    {
        $this->postalCode = $this->failedRequest ? $this->config['default_values']['postal_code'] : $this->details->postal->code;
    }
    
    /**
     * Get latitude for IP
     * @return string
     */
    public function getLatitude() 
    {
        return $this->latitude;
    }

    /**
     * Set latitude for IP
     */
    private function setLatitude() 
    {
        $this->latitude = $this->failedRequest ? $this->config['default_values']['latitude'] : $this->details->location->latitude;
    }
    
    /**
     * Get longitude for IP
     * @return string
     */
    public function getLongitude() 
    {
        return $this->longitude;
    }
    
    /**
     * Set longitude for IP
     */
    private function setLongitude() 
    {
        $this->longitude = $this->failedRequest ? $this->config['default_values']['longitude'] : $this->details->location->longitude;
    }
    
    /**
     * Get all location details for given IP address
     * @return array 
     */
    public function getLocationDetails()
    {
        $details = [
            'ip'            => $this->getIp(),
            'country_code'  => $this->getCountryCode(),
            'country_name'  => $this->getCountryName(),
            'city'          => $this->getCity(),
            'postal_code'   => $this->getPostalCode(),
            'latitude'      => $this->getLatitude(),
            'longitude'     => $this->getLongitude(),
        ];
        return $details;
    }
}