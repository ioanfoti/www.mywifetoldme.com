<?php

namespace MyWife\Libraries;

class Encryption
{
    private $encryptionData;

    public function __construct()
    {
        $this->encryptionData = \AppContainer::getInstance()['config']->getFile('encryption');
    }
    
   /**
     * Encrypt string
     * @param $plainString
     * @return string
     */
    public function encrypt($plainString)
    {
        $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);
        $cipherText = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->encryptionData['hash_key'], $plainString, MCRYPT_MODE_CBC, $iv);
        return base64_encode($iv . $cipherText);
    }

    /**
     * Decrypt string
     * @param $encryptedString
     * @return string
     */
    public function decrypt($encryptedString)
    {
        $cipherTextDec = base64_decode($encryptedString);
        $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $ivDec = substr($cipherTextDec, 0, $ivSize);
        $cipherTextDec = substr($cipherTextDec, $ivSize);
        $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->encryptionData['hash_key'], $cipherTextDec, MCRYPT_MODE_CBC, $ivDec);
        return rtrim($decrypted, "\0");
    }
}