<?php

namespace MyWife\Libraries;

use MyWife\Dto\EmailDto;

class EmailService
{
    private $emailConfig;
    private $logger;
    
    public function __construct()
    {
        $this->emailConfig = \AppContainer::getInstance()['config']->getFile('email');
        $this->logger = \AppContainer::getInstance()['logger'];
    }
    
    /**
     * Send email 
     * @return array
     */
    public function send(EmailDto $emailDto)
    {
        $emailInfo = $this->getInfo();
        
        // Set transport
        $transport = \Swift_SmtpTransport::newInstance($emailInfo['mail_server']['host'], $emailInfo['mail_server']['port'], 'tls')
            ->setUsername($emailInfo['mail_server']['user'])
            ->setPassword($emailInfo['mail_server']['pass']);
        
        // Create mailer instance
        $mailer = \Swift_Mailer::newInstance($transport);
        
        // Create instance message
        $message = \Swift_Message::newInstance();
        
        // Subject
        $logMsg['email_data']['subject'] = $emailDto->getSubject();
        $message->setSubject($logMsg['email_data']['subject']);

        // Sender
        if (!empty($emailDto->getFrom()))
            $logMsg['email_data']['from'] = $emailDto->getFrom();
        else
            $logMsg['email_data']['from'] = $emailInfo['from'];   
        $message->setFrom($logMsg['email_data']['from']);
        
        // Recipients
        $logMsg['email_data']['to'] = $emailDto->getTo();
        $message->setTo($logMsg['email_data']['to']);
        
        // Cc recipients
        if (!empty($emailDto->getCc())) {
            $logMsg['email_data']['cc'] = $emailDto->getCc();
            $message->setCc($logMsg['email_data']['to']);
        }
        
        // Bcc recipients
        if (!empty($emailDto->getBcc())) {
            $logMsg['email_data']['bcc'] = $emailDto->getBcc();
            $message->setBcc($logMsg['email_data']['bcc']);
        }
        
        // Format type
        if (!empty($emailDto->getFormatType()))
            $logMsg['email_data']['format_type'] = $emailDto->getFormatType();
        else
            $logMsg['email_data']['format_type'] = $emailInfo['format_type'];
        $message->setContentType($logMsg['email_data']['format_type']);
        
        // Body
        $message->setBody($this->getBody($emailDto, $emailInfo));
       
        // Send mailer
        $startIndex = microtime(true);
        try
        {
            $result = $mailer->send($message);
            $logMsg['result'] = $result === 1 ? 'succeed' : 'failed';
        } 
        catch (\Exception $e) {
            $exception = [
                'code'      => $e->getCode(),
                'message'   => $e->getMessage(),
            ];
            $logMsg = $exception + $logMsg;
        }
        $logMsg['email_data']['smtp_server'] = $emailInfo['mail_server']['name'];
        $logMsg['execution_time'] = number_format(microtime(true) - $startIndex, 2) .'s';

        $this->logger->info("Email sent", $logMsg);
        
        return $result === 1 ? true : false;
    }
    
    /**
     * Get mail server & sender details for email
     * @return array
     */
    private function getInfo()
    {
        $account = $this->emailConfig['mail_server'][$this->emailConfig['smtp_server']];

        $info['mail_server'] = [
            'name' => $account['name'],
            'host' => $account['host'],
            'user' => $account['user'],
            'pass' => $account['pass'],
            'port' => $account['port'],
        ];
        $info['from'] = [$this->emailConfig['contact']['from']['email'] => $this->emailConfig['contact']['from']['name']];
        $info['template'] = $this->emailConfig['default_template'];
        $info['format_type'] = $this->emailConfig['default_format_type'];
        
        return $info;
    }
    
    /**
     * Get email body
     * @param EmailDto $emailDto
     * @param array $emailInfo
     * @return string
     */
    private function getBody(EmailDto $emailDto, $emailInfo)
    {
        $template = !empty($emailDto->getTemplate()) ? 
                    $emailDto->getTemplate() :
                    \AppContainer::getInstance()['general_values']->get()['root_path'] . $emailInfo['template'];
        
        $body = !empty($emailDto->getBody()) ? $emailDto->getBody() : '';
        
        ob_start();
        require_once($template);
        $result = ob_get_clean();
        
        if (!empty($parameters = $emailDto->getParameters()))
            foreach($parameters as $key => $value) {
                $result = str_replace('{{'. $key .'}}', $value, $result);
            }
        
        return $result;
    }
}