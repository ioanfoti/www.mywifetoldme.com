<?php 

namespace MyWife\Dto;

class EmailDto
{
    protected $subject;
    protected $from;
    protected $to;
    protected $cc;
    protected $bcc;
    protected $formatType;
    protected $body;
    protected $parameters;
    protected $template;


    /**
     * Get the subject of the email
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set the subject of the email
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }
    
    /**
     * Get email senders 
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set email senders
     * @param mixed $from String for email only | Array: single value for email only | key => value for email => name
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }
    
    /**
     * Get email recipients
     * @param mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set email recipients
     * @param mixed String for email only | Array: single value for email only | key => value for email => name
     */
    public function setTo($to)
    {
        $this->to = $to;
    }
    
    /**
     * Get cc email recipients
     * @return mixed
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * Set cc email recipients
     * @param mixed $cc String for email only | Array: single value for email only | key => value for email => name
     */
    public function setCc($cc)
    {
        $this->cc = $cc;
    }
    
    /**
     * Get bcc email recipients
     * @return mixed
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * Set bcc email recipients
     * @param mixed $bcc String for email only | Array: single value for email only | key => value for email => name
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;
    }
    
    /**
     * Get email format type
     * @param string
     */  
    public function getFormatType() 
    {
        return $this->formatType;
    }

    /**
     * Set email format type
     * @param string $formatType Values: text/plain | text/html
     */
    public function setFormatType($formatType) 
    {
        $this->formatType = $formatType;
    }
    
    /** 
     * Get email body
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set email body
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * Get parameters for email body
     * @return array
     */
    public function getParameters() 
    {
        return $this->parameters;
    }

    /**
     * Set parameters for email body
     * @param array $parameters Keys will be replace with values in email body
     */
    public function setParameters($parameters) 
    {
        $this->parameters = $parameters;
    }
    
    /**
     * Get template filepath for email
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set template filepath for email
     * @param mixed $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }
}