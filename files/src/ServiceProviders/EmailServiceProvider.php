<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use MyWife\Libraries\EmailService;

class EmailServiceProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['email_service'] = function () {
            return new EmailService();
        };
    }
}