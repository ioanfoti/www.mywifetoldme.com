<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use MyWife\Libraries\Formatters;

class FormattersProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['formatters'] = function () {
            return new Formatters();
        };
    }
}