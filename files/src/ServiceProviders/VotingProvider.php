<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use MyWife\Libraries\Voting;

class VotingProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['voting'] = function () {
            return new Voting();
        };
    }
}