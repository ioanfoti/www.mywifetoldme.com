<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use MyWife\Libraries\EmailDtoFactory;

class EmailDtoFactoryProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['email_dto_factory'] = function () {
            return new EmailDtoFactory();
        };
    }
}