<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use MyWife\Libraries\WordpressRequests;

class WordpressRequestsProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['wordpress'] = function () {
            return new WordpressRequests();
        };
    }
}