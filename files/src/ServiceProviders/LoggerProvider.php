<?php

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Processor\IntrospectionProcessor;
use MyWife\Libraries\Processors\UniqueIdProcessor;

class LoggerProvider implements ServiceProviderInterface
{
    private $logPath;

    /**
     * @param string $logPath string Path to log folder
     */
    public function __construct($logPath)
    {
        $this->logPath = $logPath;
    }

    public function register(Container $c)
    {
        $c['logger_name'] = 'default';
        $c['logger_level'] = Logger::DEBUG;
        $c['logger_path'] = $this->logPath;
        $c['logger_filename'] = 'log.php';
        $c['logger_date_format'] = 'Y-m-d H:i:s';
        $c['logger_formatted_output'] = "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n\n";

        $c['logger'] = function ($c) {

            $format = "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n\n";
            $uniqueId = sha1(uniqid(sha1(str_shuffle(microtime())), true));

            $stream = new RotatingFileHandler($c['logger_path'] . '/' . $c['logger_filename'], 0, $c['logger_level']);
            $stream->setFormatter(new \Monolog\Formatter\LineFormatter($format));
            $logger = new Logger($c['logger_name']);

            $logger->pushHandler($stream);
            $logger->pushProcessor(new UniqueIdProcessor($uniqueId));
            $logger->pushProcessor(new IntrospectionProcessor(Logger::WARNING, array('Monolog\\')));

            return $logger;
        };
    }
}