<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use MyWife\Libraries\Encryption;

class EncryptionProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['encryption'] = function () {
            return new Encryption();
        };
    }
}