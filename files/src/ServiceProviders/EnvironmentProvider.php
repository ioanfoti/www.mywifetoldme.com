<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use MyWife\Libraries\Environment;

class EnvironmentProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['environment'] = function () {
            return new Environment();
        };
    }
}