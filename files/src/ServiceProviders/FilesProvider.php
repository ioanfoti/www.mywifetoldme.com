<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use MyWife\Libraries\Files;

class FilesProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['files'] = function () {
            return new Files();
        };
    }
}