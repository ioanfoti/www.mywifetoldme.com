<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use MyWife\Libraries\Config;

class ConfigProvider implements ServiceProviderInterface
{
    private $configFolder;

    /**
     * @param string $configFolder Path to root folder
     */
    public function __construct($configFolder)
    {
        $this->configFolder = $configFolder;
    }
    
    public function register(Container $c)
    {
        $c['config'] = function () {
            return new Config($this->configFolder);
        };
    }
}