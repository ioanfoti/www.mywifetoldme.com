<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use MyWife\Libraries\Cookies;

class CookiesProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['cookies'] = function () {
            return new Cookies();
        };
    }
}