<?php

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use MyWife\Libraries\GeneralValues;

class GeneralValuesProvider implements ServiceProviderInterface
{    
    public function register(Container $c)
    {
        $c['general_values'] = function () {
            return new GeneralValues;
        };
    }
}