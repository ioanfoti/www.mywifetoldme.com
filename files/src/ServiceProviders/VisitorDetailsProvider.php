<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use MyWife\Libraries\VisitorDetails;

class VisitorDetailsProvider implements ServiceProviderInterface
{
    private $cityDb;

    /**
     * @param string $cityDb Path to the Max Mind city DB
     * @param string $visitorDetailsConfig Visitor details configuration file
     */
    public function __construct($cityDb)
    {
        $this->cityDb = $cityDb;
    }
    
    public function register(Container $c)
    {
        $c['visitor_details'] = function () {
            return VisitorDetails::getInstance($this->cityDb);
        };
    }
}