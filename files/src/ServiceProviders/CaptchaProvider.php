<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use MyWife\Libraries\Captcha;

class CaptchaProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['captcha'] = function () {
            return new Captcha();
        };
    }
}