<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Predis\Client;

class PredisProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['predis'] = function () {
            return new Client(); 
        };
    }
}