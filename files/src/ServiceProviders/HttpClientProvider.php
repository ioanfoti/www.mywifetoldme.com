<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use GuzzleHttp\Client;

class HttpClientProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['http_client'] = function () {
            return new Client();
        };
    }
}