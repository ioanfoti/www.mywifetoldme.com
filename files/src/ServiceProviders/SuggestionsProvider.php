<?php 

namespace MyWife\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use MyWife\Libraries\Suggestions;

class SuggestionsProvider implements ServiceProviderInterface
{
    public function register(Container $c)
    {
        $c['suggestions'] = function () {
            return new Suggestions();
        };
    }
}