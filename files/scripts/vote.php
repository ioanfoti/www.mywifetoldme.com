<?php
// Load composer autoload
include '../../vendor/autoload.php';

$referer = \AppContainer::getInstance()['general_values']->get()['http_referer'];
$domain = \AppContainer::getInstance()['general_values']->get()['domain'];

if (strpos($referer, $domain) === false)
    exit();

// Decrypt data
$decryptedData = \AppContainer::getInstance()['voting']->decryptData($_POST['id']);

// Validate data
$validation = \AppContainer::getInstance()['voting']->validateData($decryptedData);
if ($validation['code'] != '200') {
    header('result: '. $validation['code']);
    exit();
}

// Submit vote
$votingResult = \AppContainer::getInstance()['voting']->submit($decryptedData);

// Already voted
if ($votingResult['code'] == \AppContainer::getInstance()['config']->getFile('voting')['responses']['already_voted']['code']) {
    header('result: '. $votingResult['code']);
    header('voteClass: '. $votingResult['voteClass']);
    exit();
}

header('result: '. $votingResult['code']);
exit();