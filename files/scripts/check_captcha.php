<?php
// Load composer autoload
include '../../vendor/autoload.php';

$referer = \AppContainer::getInstance()['general_values']->get()['http_referer'];
$domain = \AppContainer::getInstance()['general_values']->get()['domain'];

if (strpos($referer, $domain) === false)
    exit();

session_start();
if (isset($_REQUEST['captcha']))
    exit(json_encode(strtolower($_REQUEST['captcha']) == strtolower($_SESSION['captcha'])));

exit(json_encode(true));