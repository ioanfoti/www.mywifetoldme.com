<?php
// Load composer autoload
include '../../vendor/autoload.php';

$referer = \AppContainer::getInstance()['general_values']->get()['http_referer'];
$domain = \AppContainer::getInstance()['general_values']->get()['domain'];

if (strpos($referer, $domain) === false)
    exit();

// Save captcha string to session
session_start();
$captchaString = \AppContainer::getInstance()['captcha']->createString();
$_SESSION['captcha'] = $captchaString;

// Create captcha image
$image = \AppContainer::getInstance()['captcha']->createImage($captchaString);

// Return image
header("Content-type: image/png");
imagepng($image);