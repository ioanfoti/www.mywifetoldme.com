<!DOCTYPE html>
<html lang="en">
<head>
<?php
    include $general['root_path'] .'/files/views/includes/header/meta.php';
    include $general['root_path'] .'/files/views/includes/header/links.php';
    wp_head();
?>
</head>
<body>
    <?php include $general['root_path'] .'/files/views/includes/header/body.php';