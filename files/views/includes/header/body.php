<header>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-push-3">
                <div class="title">
                    <a href="<?php echo $general['home_link']; ?>">
                        My Wife Told Me
                    </a>
                </div>
            </div>
            <div class="col-md-12">
                <ul class="nav">
                    <li>
                        <a href="<?php echo $general['home_link']; ?>">Home</a>
                    </li>
                    <li>
                        <a href="<?php echo $general['home_link']; ?>/history">History</a>
                    </li>
                    <li>
                        <a href="<?php echo $general['home_link']; ?>/happened-to-me">Happened to me</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-12">
                <?php include $general['root_path'] .'/files/views/includes/header/addthis.php'; ?>
            </div>
        </div>
    </div>
</header>
<div class="dis50"></div>