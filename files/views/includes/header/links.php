<?php
// Favico
HTML::style($general['file_hosting_path'] .'/files/assets/img/common/favicon.ico', ['rel' => 'shortcut icon', 'type' => 'image/x-icon']);

// Google fonts
if ($general['country_code'] != 'CN')
    HTML::style('//fonts.googleapis.com/css?family=Pacifico', ['type' => 'text/css']);

HTML::style('//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css', ['integrity' => 'sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==', 'crossorigin' => 'anonymous']);
HTML::style($general['file_hosting_path'] .'/files/assets/css/custom/main.css?ver='. $general['file_versions']['css_main']);
HTML::style($general['file_hosting_path'] .'/files/assets/css/custom/queries.css?ver='. $general['file_versions']['css_queries']);