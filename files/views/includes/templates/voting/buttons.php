<?php if (empty($votingData['already_voted'])) { ?>
    <div class="vote_wrapper">
        <div id="<?php echo $votingData['encrypt_up_vote']; ?>" class="up vote">
            <div class="btn btn-success">
                Already Heard | <span class="up-votes"><?php echo $votingData['already_heard']; ?></span>
            </div>
        </div>
        <div id="<?php echo $votingData['encrypt_down_vote']; ?>" class="down vote">
            <div class="btn btn-danger">
                Not Yet | <span class="down-votes"><?php echo $votingData['not_yet']; ?></span>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="vote_wrapper">
        <div class="up">
            <div class="btn btn-success <?php echo $votingData['up_voted']; ?> inactive">
                Already Heard | <span class="up-votes"><?php echo $votingData['already_heard']; ?></span>
            </div>
        </div>
        <div class="down">
            <div class="btn btn-danger <?php echo $votingData['down_voted']; ?> inactive">
                Not Yet | <span class="down-votes"><?php echo $votingData['not_yet']; ?></span>
            </div>
        </div>
    </div>
<?php }