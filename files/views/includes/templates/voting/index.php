<?php
$votingData = \AppContainer::getInstance()['voting']->getVotingPostData($post['ID'], $general['ip_address'], $general['country_code']);

if (!empty($votingData['voting_script']))
    $scripts['submit_vote'] = true;
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h4>
                <span title="Publish date" class="glyphicon glyphicon-calendar"></span>
                <?php echo strstr($post['post_date_gmt'], ' ', true); ?>
            </h4>
            <h2>
                <?php echo $post['post_content']; ?>
            </h2>
        </div>
    </div>
    <div class="dis30"></div>
    
    <div class="row">
        <div class="col-md-12">
            <?php include $general['root_path'] .'/files/views/includes/templates/voting/buttons.php'; ?>
        </div>
    </div>
    <div class="dis60"></div>
    
    <div class="row">
        <div class="col-md-6">
            <?php include $general['root_path'] .'/files/views/includes/scripts/disqus.php'; ?>
        </div>
    </div>
</div>