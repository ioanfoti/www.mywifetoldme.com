<div class="row">
    <div class="col-md-12">
        <form id="suggestions" class="form" action="<?php echo $general['home_link']; ?>/happened-to-me#result" method="post">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group has-feedback">
                        <label for="phrase">
                            <span class="red">*</span>
                            <?php echo $messages['my_wife_told_me']; ?>
                        </label>
                        <span class="chars-remaining hidden pull-right">
                            <span class="chars-number"></span>
                            <?php echo $messages['characters_left']; ?>
                        </span>
                        <textarea name="phrase" id="phrase" class="form-control" rows="4"><?php echo $phrase; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="dis20"></div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group has-feedback">
                        <label for="tag">
                            <?php echo $messages['email'] .' '. $messages['optional']; ?>
                        </label>
                        <span class="whatis hidden-xs hidden-sm pull-right" title="<?php echo $messages['email_tooltip']; ?>">
                            <?php echo $messages['what_is_this']; ?>
                        </span>
                        <input type="text" name="email" id="email" class="form-control" value="<?php echo $email; ?>">
                    </div>
                </div>
            </div>
            <div class="dis30"></div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group has-feedback">
                        <label for="tag">
                            <?php echo $messages['tag'] .' '. $messages['optional']; ?>
                        </label>
                        <span class="whatis hidden-xs hidden-sm pull-right" title="<?php echo $messages['tag_tooltip']; ?>">
                            <?php echo $messages['what_is_this']; ?>
                        </span>
                        <input type="text" name="tag" id="tag" class="form-control" value="<?php echo $tag; ?>">
                    </div>
                </div>
            </div>
            <div class="dis30"></div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group has-feedback">
                        <label for="captcha">
                            <span class="red">*</span>
                            <?php echo $messages['captcha']; ?>
                        </label>
                        <div id="captcha-wrap">
                            <i class="fa fa-refresh" id="refresh-captcha"></i>
                            <img src="/files/scripts/new_captcha.php" alt="" id="captcha">
                        </div>
                        <input type="text" name="captcha" id="captcha" class="form-control" value="<?php echo $captcha; ?>">
                    </div>
                </div>
            </div>
            <div class="dis30"></div>
            
            <div class="row">
                <div class="col-md-6">
                    <button type="submit" id="submit-btn" class="btn btn-solid btn-success">
                        <?php echo $messages['submit']; ?>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>