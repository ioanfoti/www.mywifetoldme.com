<?php
$votingPostsData = \AppContainer::getInstance()['voting']->getAllVotingPostsData($general['ip_address'], $general['country_code']);
$id = 1;
foreach($votingPostsData as $votingData) {
    if (!empty($votingData['voting_script']))
        $scripts['submit_vote'] = true;
?>
    <div id="<?php echo $id; ?>" class="row voting-post mt30">
        <div class="col-md-3">
            <span class="sort-title">
            <?php echo '<a href="'. $general['home_link'] .'/'. $votingData['url'] .'">'. $votingData['title'] .'</a>'; ?>
            </span>
        </div>
        <div class="col-md-3">
            <span class="sort-date">
                <?php echo $votingData['publish_date']; ?> 
            </span>
        </div>
        <div class="col-md-3">
            <span class="sort-votes">
                <?php echo $votingData['already_heard'] + $votingData['not_yet']; ?>
            </span>
        </div>
        <div class="col-md-3">
            <?php include $general['root_path'] .'/files/views/includes/templates/voting_history/buttons.php'; ?>
        </div>
    </div>
<?php 
    $id++;
}