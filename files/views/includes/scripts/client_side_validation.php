<script>
$(document).ready(function() {

    var pleaseWait = "<?php echo $messages['please_wait']; ?>";

    $.validator.setDefaults({ ignore: '' });
    $('.btn-group .btn.btn-default').on('change', function() {
        $(this).find('input').valid();
    });

    <?php if (isset($clientSideRules)) { ?>

        $('form<?php echo $clientSideRules['selector']; ?>').each(function () {

            $(this).validate({
                
                <?php
                if (!empty($clientSideRules['groups'])) { ?>
                groups: {
                    <?php foreach($clientSideRules['groups'] as $key => $value) {
                        echo $key .": '". implode(' ', $value['fields']) ."'";
                    } ?>
                },
                <?php } ?>

                rules: {
                    <?php
                    foreach($clientSideRules['rules'] as $key => $value) {
                        echo $key .': { ';
                        foreach($value as $rule) {
                            echo $rule['name'] .': '. $rule['value'] .', ';
                        }
                        echo ' }, ';
                    }
                    ?>
                },

                messages: {
                    <?php
                    foreach($clientSideRules['rules'] as $key => $value) {
                        echo $key .': { ';
                        foreach($value as $rule) {
                            if (isset($rule['message']))
                                echo $rule['name'] .': "'. $rule['message'] .'", ';
                        }
                        echo ' }, ';
                    }
                    ?>
                },

                highlight: function (element, errorClass, validClass) {
                    if ($(element).attr('type') == 'file') {
                        var icon = '<div class="glyphicon-wrapper"><span aria-hidden="true" class="glyphicon glyphicon-remove form-control-feedback"></span></div>';
                        $(element)
                            .closest('.input-group')
                            .removeClass('has-success')
                            .addClass('has-error')
                            .end()
                            .closest('.input-group-btn')
                            .siblings('.glyphicon-wrapper').remove()
                            .end()
                            .closest('.input-group-btn')
                            .before(icon);
                    }
                    else {
                        $(element)
                            .addClass(errorClass)
                            .removeClass(validClass);

                        if ($(element).attr('type') != 'checkbox' && $(element).attr('type') != 'radio') {
                            var selectClass = ($(element).prop('type') == 'select-one') ? ' select' : '',
                                isibling = ($(element).siblings('i').length > 0) ? ' isibling' : '',
                                icon = '<div class="glyphicon-wrapper"><span aria-hidden="true" class="glyphicon glyphicon-remove form-control-feedback' + selectClass + isibling + '"></span></div>';
                            $(element)
                                .closest('.form-group')
                                .removeClass('has-success')
                                .addClass('has-error')
                                .end()
                                .siblings('.glyphicon-wrapper').remove()
                                .end()
                                .before(icon);
                        }
                    }
                },
               
                unhighlight: function (element, errorClass, validClass) {

                    if ($(element).attr('type') == 'file') {
                        var icon = '<div class="glyphicon-wrapper"><span aria-hidden="true" class="glyphicon glyphicon-ok form-control-feedback"></span></div>';
                        $(element)
                            .closest('.input-group')
                            .removeClass('has-error')
                            .addClass('has-success')
                            .end()
                            .closest('.input-group-btn')
                            .siblings('.glyphicon-wrapper').remove()
                            .end()
                            .closest('.input-group-btn')
                            .before(icon)

                        if ($(element).hasClass('file_upload_group')) {
                            $('.file_upload_group').each(function() {
                                if (!$(this).val()) {
                                    $(this)
                                        .closest('.input-group')
                                        .removeClass('has-error')
                                        .removeClass('has-success')
                                        .end()
                                        .closest('.input-group-btn')
                                        .siblings('.glyphicon-wrapper').remove()
                                        .end()
                                        .closest('.input-group')
                                        .siblings('label.error').remove();
                                }
                            });
                        }
                    }
                    else {
                        $(element)
                            .removeClass(errorClass)
                            .addClass(validClass);

                        if ($(element).attr('type') != 'checkbox' && $(element).attr('type') != 'radio' && $(element).attr('type') != 'file' && !($(element).attr('type') == 'text' && $(element).siblings('.input-group-btn').length > 0)) {
                            var selectClass = ($(element).prop('type') == 'select-one') ? ' select' : '',
                                isibling = ($(element).siblings('i').length > 0) ? ' isibling' : '',
                                icon = '<div class="glyphicon-wrapper"><span aria-hidden="true" class="glyphicon glyphicon-ok form-control-feedback' + selectClass + isibling + '"></span></div>';
                                
                            $(element)
                                .closest('.form-group')
                                .removeClass('has-error')
                                .addClass('has-success')
                                .end()
                                .siblings('.glyphicon-wrapper').remove()
                                .end()
                                .before(icon);
                        }
                    }
                },

                errorPlacement: function (error, element) {
                    <?php
                    if (!empty($clientSideRules['groups'])) {
                        foreach($clientSideRules['groups'] as $key => $value) {
                    ?>
                    if ($.inArray(element.attr('name'),['<?php echo implode("', '", $value['fields']); ?>']) != -1) {
                        var errorSelector = $('[name="<?php echo end($value['fields']);?>"]').closest('.row');
                        error
                            .appendTo(errorSelector)
                            .wrap('<div class="col-sm-12 col-md-12 up15"></div>');
                        return true;
                    }
                    <?php
                        }
                    }
                    ?>

                    if (element.siblings('.input-group-addon').length > 0) {
                        $(element)
                            .closest('.input-group')
                            .after(error);
                    }

                    else if (element.closest('.form-group').find('.btn-group').length > 0) {
                        $(element)
                            .closest('.form-group').find('.btn-group')
                            .after(error);
                    }

                    else if (element.attr('type') == 'file') {
                        $(element)
                            .closest('.input-group')
                            .after(error);
                    }

                    else if (element.attr('type') == 'checkbox') {
                        $(element)
                            .closest('.form-group')
                            .append(error);
                    }

                    else if (element.closest('.radio-group').find('.radio').length > 0) {
                        $(element)
                            .closest('.radio-group')
                            .after(error);
                    }

                    else if (element.attr('type') == 'radio') {
                        $(element)
                            .closest('label')
                            .siblings('span')
                            .after(error);
                    }

                    else {
                        error.insertAfter(element);
                    }
                },

                invalidHandler: function (form, validator) {
                    scrollToObject($(validator.errorList[0].element));
                },

                submitHandler: function (form) {
                    $(form).find('button[type="submit"]').text(pleaseWait);
                    form.submit();
                    return false;
                }

            });

        });
    
    <?php } ?>
    
});
</script>