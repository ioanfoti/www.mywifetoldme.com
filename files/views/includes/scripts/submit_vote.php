<script>
$(document).ready(function() {      

    $(".vote").on('click', function() {
        var data = {id: $(this).attr('id')},
            voteClass = $(this).attr('class'),
            wrapper = $(this).closest('.vote_wrapper'),
            url = "/files/scripts/vote.php",
            result;

        $.ajax({
            url: url,
            data: data,
            type: 'post',
            async: false,
            success: function (data, status, xhr) {
                result = xhr.getResponseHeader('result');
                if (result == 409)
                    voteClass = xhr.getResponseHeader('voteClass')
                if (result == 201 || result == 409)
                    voteSuccess(wrapper, voteClass);
            }
        });
    });
    
    function voteSuccess(wrapper, voteClass) {
        var upWrapper = wrapper.find('.up'),
            downWrapper = wrapper.find('.down'),
            upVotesWrapper = wrapper.find('.up-votes'),
            downVotesWrapper = wrapper.find('.down-votes'),
            upVotesButton = upWrapper.find('.btn'),
            downVotesButton = downWrapper.find('.btn');
    
        if (voteClass.indexOf('up') >= 0) {
            upVotesWrapper.text(parseInt(upVotesWrapper.text()) + 1);
            upVotesButton.addClass('voted');
        }
        else {
            downVotesWrapper.text(parseInt(downVotesWrapper.text()) + 1);
            downVotesButton.addClass('voted');
        }
        
        if ($('.sort-votes').length > 0) {
            var sortVotes = wrapper.parent().siblings().find('.sort-votes');
            sortVotes.text(parseInt(sortVotes.text()) + 1);
        }
        
        upVotesButton.addClass('inactive');
        downVotesButton.addClass('inactive');
        
        upWrapper.removeClass('vote').removeAttr('id').off('click');
        downWrapper.removeClass('vote').removeAttr('id').off('click');
    }
            
});
</script>