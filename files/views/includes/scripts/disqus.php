<div id="disqus_thread"></div>
<script>
    var disqus_config = function () {
        this.page.url = "<?php echo $general['home_link'] .'/'. $post['post_name']; ?>";
        this.page.identifier = "<?php echo $post['ID'] .' '. $post['guid']; ?>";
        this.page.title = "<?php echo $post['post_title']; ?>";
    };

    (function() {
        var d = document, s = d.createElement('script');
        
        s.src = '//mywifetoldme.disqus.com/embed.js';
        
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>