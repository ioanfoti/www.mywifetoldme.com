<?php
$msgIcons = array(
    'warning'   => 'fa-warning',
    'info'      => 'fa-info-circle',
    'danger'    => 'fa-minus-circle',
    'success'   => 'fa-check-circle',
);
?>
<div class="<?php echo (isset($msg['class'])) ? $msg['class'] : 'col-md-12'; ?>">
    <div role="alert" class="alert alert-<?php echo $msg['type']; ?>">
        <?php
        if (!empty($msg['title'])) {
        ?>
            <div class="wrap">
                <div>
                    <i class="fa <?php echo $msgIcons[$msg['type']]; ?>"></i>
                </div>
                <div>
                    <h2>
                        <?php echo $msg['title']; ?>
                    </h2>
                </div>
            </div>
        <?php
        }

        // Get paragraphs
        $paragraphsText = '';
        if (isset($msg['paragraphs'])) {
            foreach($msg['paragraphs'] as $paragraph) {
                if (!empty($paragraph))
                    $paragraphsText .= '<p>'. $paragraph .'</p>';
            }
        }

        // Get list
        $listText = '';
        if (isset($msg['list'])) {
            $listText .= '<ul class="arrows">';
            foreach($msg['list'] as $item) {
                if (!empty($item))
                    $listText .= '<li>'. $item .'</li>';
            }
            $listText .= '</ul>';
        }

        // Display paragraphs & text
        if (isset($msg['paragraph_after']))
            echo $listText .'<div class="dis20"></div>'. $paragraphsText;
        else
            echo $paragraphsText . $listText;

        // Button
        if (isset($msg['button']['url'])) {
        ?>
        <div class="row top20">
            <div class="col-md-6">
                <a class="btn btn-solid <?php echo isset($msg['button']['class']) ? $msg['button']['class'] : 'btn-red'; ?> btn-block" href="<?php echo $msg['button']['url']; ?>">
                    <?php echo $msg['button']['text']; ?>
                </a>
            </div>
        </div>
        <?php } ?>
    </div>
</div>