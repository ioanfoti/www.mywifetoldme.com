<?php
HTML::script('//code.jquery.com/jquery-1.11.3.min.js');
HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js', ['integrity' => 'sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==', 'crossorigin' => 'anonymous']);

HTML::script($general['file_hosting_path'] .'/files/assets/js/custom/main.js?ver='. $general['file_versions']['js_main']);

if (!empty($javascripts['sorter']))
    HTML::script($general['file_hosting_path'] .'/files/assets/js/custom/sorter.js?ver='. $general['file_versions']['js_sorter']);

if (!empty($javascripts['search_filter']))
    HTML::script($general['file_hosting_path'] .'/files/assets/js/custom/search_filter.js?ver='. $general['file_versions']['js_search_filter']);

if (!empty($javascripts['form'])) {
    HTML::script('//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js');
    HTML::script('//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/additional-methods.min.js');
    HTML::script('//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js');
    HTML::script($general['file_hosting_path'] .'/files/assets/js/custom/custom_methods.js');
    include $general['root_path'] .'/files/views/includes/scripts/client_side_validation.php';
}

if (!empty($javascripts['bootstrap']))
    HTML::script('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js');

if (!empty($javascripts['suggestions']))
    HTML::script($general['file_hosting_path'] .'/files/assets/js/custom/suggestions.js');

if (!empty($scripts))
    foreach($scripts as $key => $value) {
        include $general['root_path'] .'/files/views/includes/scripts/'. $key .'.php';
    }
    
include $general['root_path'] .'/files/views/includes/scripts/analytics.php';

include $general['root_path'] .'/files/views/includes/scripts/addthis.php';