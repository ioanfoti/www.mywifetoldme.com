<div class="dis50"></div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <p><strong>Terms of use:</strong> By using our website you accept that the character of this website is clearly humorous and does not meant to offend anyone.</p>
                <div class="copyright">
                    &copy; 2015 MyWifeToldMe | <a href="mailto: info@mywifetoldme.com">Contact us</a>
                </div>
            </div>
        </div>
    </div>
</footer>