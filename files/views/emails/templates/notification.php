<?php
$rootPath = \AppContainer::getInstance()['general_values']->get()['root_path'];
include $rootPath .'/files/views/emails/header/main.php';
?>
<h3>Client details</h3>
<strong>IP Address:</strong> {{ip_address}}<br>
<strong>Country:</strong> {{country}}<br>

<h3>Notification Message</h3>
<p><?php echo $body; ?></p>

<?php
include $rootPath .'/files/views/emails/footer/main.php';