$(document).ready(function() {
    var fieldSelector = $('#phrase'),
        minChars = 5,
        maxChars = 100,
        charsSelector = fieldSelector.closest('form').find('.chars-remaining'),
        numberSelector = charsSelector.find('.chars-number'),
        typedChars = fieldSelector.val().length,
        remainingChars = maxChars - typedChars;  
  
    charsSelector.removeClass('hidden');
    numberSelector.text(remainingChars);

    fieldSelector.on('keyup change', function() {
        var typedChars = $(this).val().length,
            remainingChars = maxChars - typedChars;
        numberSelector.text(remainingChars);
        changeStatus(charsSelector, typedChars, minChars, maxChars);
    });
    
    $('.whatis').tooltip({
        placement: 'bottom'
    });
          
    $('#refresh-captcha').on('click', function() {
        $('img#captcha').attr('src', '/files/scripts/new_captcha.php?rnd=' + Math.random());
        var captcha = $('[name="captcha"]');
        if (captcha.val() != '' && captcha.val() != 0) {
            captcha.removeData('previousValue');
            captcha.valid();
        }
    });
    
    function changeStatus(selector, typed, minAllowed, maxAllowed) {
        if (minAllowed > typed || typed > maxAllowed)
            selector.addClass('error-msg').removeClass('success-msg');
        else
            selector.addClass('success-msg').removeClass('error-msg');
    }
});