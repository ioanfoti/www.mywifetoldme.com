$(document).ready(function() {
    var sortItem = 'div',
        sortingClass = 'sorting',
        sortingAscClass = 'sorting_asc',
        sortingDescClass = 'sorting_desc';
    
    if ($('.' + sortingAscClass).length > 0) 
        $('body').data('sortType' + $('.' + sortingAscClass).attr('id'), 'desc');
    else if ($('.' + sortingDescClass).length > 0)
        $('body').data('sortType' + $('.' + sortingDescClass).attr('id'), 'asc');
    
    function sortContent(parent, sortItem, sortKeyId) {
        var sortKeyClass = '.' + sortKeyId,
            sortTypeSelector = 'sortType' + sortKeyId,
            sortTypeValue = $('body').data(sortTypeSelector),
            items = parent.children(sortItem).sort(function(a, b) {
                var vA = $(sortKeyClass, a).text();
                var vB = $(sortKeyClass, b).text();           
                if (sortTypeValue === 'asc') {
                    if ($.isNumeric(vA))
                        return vA - vB;
                    return (vA < vB) ? -1 : (vA > vB) ? 1 : 0;
                }
                else {
                    if ($.isNumeric(vA))
                        return vB - vA;
                    return (vB < vA) ? -1 : (vB > vA) ? 1 : 0;
                }
            });
        parent.append(items);
        
        $('.' + sortingAscClass).removeClass(sortingAscClass).addClass(sortingClass);
        $('.' + sortingDescClass).removeClass(sortingDescClass).addClass(sortingClass);
        $('body').removeData();

        if (sortTypeValue === 'asc') {
            $('body').data(sortTypeSelector, 'desc');
            $('#' + sortKeyId).addClass(sortingAscClass);
        }
        else {
            $('body').data(sortTypeSelector, 'asc');
            $('#' + sortKeyId).addClass(sortingDescClass);
        }
    }

    $('.' + sortingClass + ', .' + sortingAscClass + ', .' + sortingDescClass).click(function() {
        var parentId = $(this).closest('.sorting-wrapper').find('.sorting-content').attr('id'),
            sortKeyId = $(this).attr('id');
        sortContent($('#' + parentId), sortItem, sortKeyId);
    });  
});