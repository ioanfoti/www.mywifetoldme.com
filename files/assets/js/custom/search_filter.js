$(document).ready(function() {
    $('#filter').keyup(function () {
        var filter = this.value.toLowerCase(),
            filterArray = new Array(),
            numberOfFilters;

        filterArray = filter.split(' ');
        numberOfFilters = filterArray.length;
        
        $('.voting-post').each(function() {
            var title = $(this).find('.sort-title').text().toLowerCase(),
                hiddenPost = 0;

            for (var i=0; i<numberOfFilters; i++) {
                if (title.indexOf(filterArray[i]) < 0) {
                    $(this).hide();
                    hiddenPost = 1;
                }
            }

            if (hiddenPost === 0)
               $(this).show();
        });
    });
});