$(document).ready(function() {
    var currentUrl = window.location.href;
    
    if (currentUrl.substr(-1) === '/')
        currentUrl = currentUrl.substr(0, currentUrl.length - 1);
    
    $('header li a').each(function() {
        if ($(this).attr('href') == currentUrl)
            $(this).addClass('active');
    });
     
    $('input[type!="file"], select, textarea').each(function(){
        $(this).on('change', function(){
            $(this).val($.trim($(this).val()));
        });
    });
});

function scrollToObject(obj) {
    $('html, body').animate({
        scrollTop: obj.offset().top - 40
    }, 500);
}