<?php

return [
    'number_of_characters'  => 5,
    'img_width'             => 200,         // Pixels
    'img_height'            => 50,          // Pixels
    'background_color'      => '#8370FF',   // Hex
    'text_color'            => '#fff',      // Hex
    'coordinates'           => [
        'x1' => 0,
        'y1' => 0,
        'x2' => 399,
        'y2' => 99,
    ],
    'image_text' => [
        'font_size'     => 30,
        'angle'         => 0,
        'x_ordinate'    => 10,
        'y_ordinate'    => 40,
        'font_name'     => 'Moms_typewriter.ttf',
    ],
];