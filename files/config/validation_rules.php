<?php
$regex = AppContainer::getInstance()['config']->getFile('regex');
$messages = AppContainer::getInstance()['config']->getFile('messages');
$formMessages = AppContainer::getInstance()['config']->getFile('form_messages');

return [

    'suggestions' => [
        'client_side' => [
            'selector' => "#suggestions",
            'rules' => [
                'phrase' => [
                    0 => [
                        'name'      => "required",
                        'value'     => "true",
                        'message'   => sprintf($formMessages['required'], $messages['my_wife_told_me'])
                    ],
                    1 => [
                        'name'      => "regex",
                        'value'     => $regex['english_characters'],
                        'message'   => sprintf($formMessages['english_characters'], $messages['my_wife_told_me'])
                    ],
                    2 => [
                        'name'      => "minlength",
                        'value'     => "5",
                        'message'   => sprintf($formMessages['min_length'], $messages['tag'], 5)
                    ],
                    3 => [
                        'name'      => "maxlength",
                        'value'     => "100",
                        'message'   => sprintf($formMessages['max_length'], $messages['tag'], 100)
                    ],
                ],
                
                'email' => [
                    0 => [
                        'name'      => "email",
                        'value'     => "true",
                        'message'   => sprintf($formMessages['email'], $messages['email'])
                    ],
                ],
                
                'tag' => [
                    0 => [
                        'name'      => "regex",
                        'value'     => $regex['english_characters'],
                        'message'   => sprintf($formMessages['english_characters'], $messages['tag'])
                    ],
                    1 => [
                        'name'      => "minlength",
                        'value'     => "5",
                        'message'   => sprintf($formMessages['min_length'], $messages['tag'], 5)
                    ],
                    2 => [
                        'name'      => "maxlength",
                        'value'     => "32",
                        'message'   => sprintf($formMessages['max_length'], $messages['tag'], 32)
                    ],
                ],
                
                'captcha' => [
                    0 => [
                        'name'      => "required",
                        'value'     => "true",
                        'message'   => sprintf($formMessages['required'], $messages['captcha'])
                    ],
                    1 => [
                        'name'      => "minlength",
                        'value'     => "5",
                        'message'   => $formMessages['captcha']
                    ],
                    2 => [
                        'name'      => "maxlength",
                        'value'     => "5",
                        'message'   => $formMessages['captcha']
                    ],
                    3 => [
                        'name'      => "remote",
                        'value'     => "'/files/scripts/check_captcha.php'",
                        'message'   => $formMessages['captcha']
                    ],   
                ],
            ],
        ],
    ],
];