<?php

return [
    
    'db' => [
        'postmeta_table'    => 'mywife_postmeta',
        'voting_template'   => 'voting.php',
        'voting_table'      => 'voting_count',
    ],
    
    'log'           => [
        'default_type'          => 'info',
        'get_votes_description' => "Get post votes",
        'submit_description'    => "Submit vote",
        'connection_error'      => "DB connection error: ",
        'post_not_exist'        => "Post ID %s does not exist OR does not use voting template",
    ],
    
    'responses' => [
        'connection_error' => [
            'code' => "500",
            'text' => "Server error",
        ],
        'bad_request' => [
            'code' => "400",
            'text' => "Bad request",
        ],
        'valid_data' => [
            'code' => "200",
            'text' => "Valid data",
        ],
        'post_not_exist' => [
            'code' => "404",
            'text' => "Not found",
        ],
        'already_voted' => [
            'code' => "409",
            'text' => "Already voted",
        ],
        'failed' => [
            'code' => "501",
            'text' => "Adding vote failed",
        ],
        'succeed' => [
            'code' => "201",
            'text' => "Adding vote succeed",
        ],
    ],
    
    'available_vote_types' => ['already_heard', 'not_yet'],
    
];