<?php

return [
    
    'country' => [
        'default_value' => 'CY',
        'name'          => 'mwtmcntr',
        'lifetime'      => strtotime('+30days'),    // 30 days
        'path'          => '/',
    ],

];