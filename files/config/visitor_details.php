<?php

return [
    
    'default_values' => [
        'country_code'  => 'CY',
        'country_name'  => 'Cyprus',
        'city'          => 'Limassol',
        'postal_code'   => '3083',
        'latitude'      => '34.6841',
        'longitude'     => '33.0379',
    ],
    
];