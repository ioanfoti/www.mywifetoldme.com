<?php

return [
    
    'something_to_share' => "Is there something which you have already heard and you want to share it?",
    'use_form_below' => "Just use the form below and send it to us!",
    'search_before_send' => "Before you submit the form, you can search in <a href=\"/history\" target=\"_blank\">History page</a> if your suggestion has already posted.",
    'my_wife_told_me' => "My Wife Told Me",
    'characters_left' => "characters left",
    'email' => "E-mail",
    'email_tooltip' => "Fill this field, if you want to receive a notification email when your suggestion will be posted",
    'tag' => "Tag",
    'tag_tooltip' => "Fill this field, if you want to add a nickname as a tag in the post",
    'what_is_this' => "What is this?",
    'optional' => "(optional)",
    'captcha' => "Verification code",
    'submit' => "Submit",
    'please_wait' => "Please wait..",
    'errors_occured' => "The following errors occurred",
    'an_error_occured' => "An error occurred",
    'an_error_occured_msg' => "An unexpected error has occurred and your submission failed. Please try again or contact us at <a href=\"mailto:support@mywifetoldme.com\">support@mywifetoldme.com</a>.",
    'already_suggested' => "Already suggested",
    'already_suggested_msg' => "You have already sent this suggestion.",
    'suggestion_sent' => "Thank you for your suggestion!",
    'suggestion_sent_msg' => "Your suggestion has been sent successfully. We will check it as soon as possible and we will include it in our future posts.",
    
];