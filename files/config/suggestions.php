<?php

return [
    
    'db' => [
        'table' => 'suggestions',
    ],
    
    'log'           => [
        'default_type'          => 'info',
        'submit_description'    => "Submit suggestion",
        'connection_error'      => "DB connection error: ",
    ],
    
    'responses' => [
        'connection_error' => [
            'code' => "500",
            'text' => "Server error",
        ],
        'already_suggested' => [
            'code' => "409",
            'text' => "Already suggested",
        ],
        'failed' => [
            'code' => "501",
            'text' => "Adding suggestion failed",
        ],
        'succeed' => [
            'code' => "201",
            'text' => "Adding suggestion succeed",
        ],
    ],
];