<?php

return [
    
    'required' => "The <strong>%s</strong> field is required",
    'email' => "The <strong>%s</strong> field must contain a valid email address",
    'min_length' => "The <strong>%s</strong> field must be at least %s characters long",
    'max_length' => "The <strong>%s</strong> field cannot exceed %s characters long",
    'range' => "The <strong>%s</strong> field must be between %s and %s characters",
    'english_characters' => "The <strong>%s</strong> field can only contain English letters (a-z, A-Z), numbers (0-9), spaces and the following characters (, . - _)",
    'captcha' => "Please enter the correct <strong>Verification code</strong>",
    
];