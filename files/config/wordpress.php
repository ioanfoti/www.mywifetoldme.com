<?php

return [
    
    'args' => [
        
        'voting' => [
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'post_type'  => 'page', 
            'meta_query' => [ 
                [
                    'key'   => '_wp_page_template', 
                    'value' => 'voting.php',
                ]
            ],
        ],
        
    ],
    
];