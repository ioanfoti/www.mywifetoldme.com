<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'AppContainer' => $baseDir . '/AppContainer.php',
    'HTML' => $baseDir . '/files/src/Helpers/HTML.php',
);
