<?php

/**
 * Disable RSD xml & wlwmanifest.xml from wp_head
 */
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');

/**
 * Disable generator from html (<meta name="generator" content="WordPress x.x" />)
 */
if (function_exists('wp_generator')) {
    function no_generator() {
        return '';
    }
    add_filter('the_generator', 'no_generator');
}

/**
 * Disable emoji style & script
 */
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');

/**
 * Disable WordPress jquery load
 */
if (!is_admin()) {
    wp_deregister_script('jquery');
}