<?php include $general['root_path'] .'/files/views/includes/header/header.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>
                Unbelievable, but it's broken!
            </h2>
        </div>
    </div>
    <div class="dis30"></div>
    
    <div class="row">
        <div class="col-md-12">
            <p>
                Looks like you are searching for something that does not exist or something that we want to hide from you. 
            </p>
            <p>
                If you entered the URL manually, then it is the first case and you could check your spelling and try again.<br>
                Otherwise, please respect the effort we put to hide this page and use our menu to navigate our site.
            </p>
        </div>
    </div>
</div>

<?php
include $general['root_path'] .'/files/views/includes/footer/footer.php';