<?php /* Template Name: Suggestions */ ?>
<?php
session_start();
include $general['root_path'] .'/files/views/includes/header/header.php'; 
$clientSideRules = AppContainer::getInstance()['config']->getFile('validation_rules')['suggestions']['client_side'];
$javascripts['form'] = true;
$javascripts['bootstrap'] = true;
$javascripts['suggestions'] = true; 
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>
                <?php echo get_the_title(); ?>
            </h2>
        </div>
    </div>
    <div class="dis30"></div>
    
    <div class="row">
        <div class="col-md-12">
            <p>
                <?php echo $messages['something_to_share']; ?>
            </p>
            <p>
                <?php echo $messages['use_form_below']; ?>
            </p>
            <p>
                <?php echo $messages['search_before_send']; ?>
            </p>
        </div>
    </div>
    <div class="dis30"></div>
        
    <?php
    $phrase = $tag = $email = $captcha = '';
    if (!empty($_POST)) {
        $suggestionData = [
            'post'          => $_POST,
            'ip_address'    => $general['ip_address'], 
            'country'       => $general['country_code'],
        ];
        $submission = AppContainer::getInstance()['suggestions']->submit($suggestionData);
        
        if ($submission['msg']['type'] != 'success') {
            $phrase = $submission['post']['phrase'];
            $email = $submission['post']['email'];
            $tag = $submission['post']['tag'];
            $captcha = $submission['post']['captcha'];
        }
        $msg = $submission['msg'];
        echo '<div id="result" class="row">';
        include $general['root_path'] .'/files/views/includes/messages/msg_box.php';
        echo '</div>';
        echo '<div class="dis50"></div>';
    }
    ?>
    
    <div class="row">
        <div class="col-md-12">
            <?php include $general['root_path'] .'/files/views/includes/templates/suggestions/form.php'; ?>
        </div>
    </div>
</div>

<?php
include $general['root_path'] .'/files/views/includes/footer/footer.php';