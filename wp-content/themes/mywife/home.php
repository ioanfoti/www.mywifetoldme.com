<?php 
include $general['root_path'] .'/files/views/includes/header/header.php'; 

$votingPosts = \AppContainer::getInstance()['wordpress']->getVotingPosts(1);
$post = $votingPosts[0];
include $general['root_path'] .'/files/views/includes/templates/voting/index.php';

include $general['root_path'] .'/files/views/includes/footer/footer.php';