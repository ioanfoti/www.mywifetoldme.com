<?php /* Template Name: Voting History */ ?>
<?php 
include $general['root_path'] .'/files/views/includes/header/header.php';
$javascripts['sorter'] = true;
$javascripts['search_filter'] = true;
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>
                <?php echo get_the_title(); ?>
            </h2>
        </div>
    </div>
</div>
<div class="dis30"></div>

<div class="sorting-wrapper container">
    <div class="row">
        <div class="col-md-1 col-md-offset-8 text-right">
            Search:
        </div>
        <div class="col-md-3">
            <input type="text" class="form-control" id="filter" name="filter" class="filter">
        </div>
    </div>
    <div class="dis20"></div>
    
    <div class="row shorting-head">
        <div id="sort-title" class="col-md-3 sorting">
            Title
        </div>
        <div id="sort-date" class="col-md-3 sorting_desc">
            Publish date
        </div>
        <div id="sort-votes" class="col-md-3 sorting">
            Total Votes
        </div>
        <div class="col-md-3">
            <span id="sort-up" class="sorting">Already heard</span> / 
            <span id="sort-down" class="sorting">Not yet</span>
        </div>
    </div>
            
    <div id="history" class="sorting-content">
        <?php include $general['root_path'] .'/files/views/includes/templates/voting_history/results.php'; ?>
    </div>
</div>

<?php 
include $general['root_path'] .'/files/views/includes/footer/footer.php';